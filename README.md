# Lightsaber

## Introduction

> This is the weapon of a Jedi Knight. Not as clumsy or random as a blaster. An elegant weapon, for a more civilized age. - Obi-Wan Kenobi
